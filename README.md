The task consists on the next steps:

Please write several test scenarios for Facebook sign-up page in a text file using Gherkin language making sure to tackle all relevant test scenarios.

Please automate the following test scenario using Cypress:

- Go to https://www.amazon.com
- Search for “hats for men”
- Add the first hat appearing to Cart with quantity 2
- Open cart and assert that the total price and quantity are correct
- Reduce the quantity from 2 to 1 in Cart for the item selected in the step 3
- Assert that the total price and quantity has been correctly changed

The goal of this test is to check if you are able to automate a test of a given website, but we’d like you to also demonstrate the coding quality, structure, and style of the deliverables. The required reporting is the console output, but having nicer reporting (HTML, hit an API, XML…) is a plus.

Please document as much as possible the setup of your deliverables assuming we would run the tests in a local machine